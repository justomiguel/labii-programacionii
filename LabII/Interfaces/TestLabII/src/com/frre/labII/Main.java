package com.frre.labII;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.frre.labII.usrs.Alumno;
import com.frre.labII.usrs.Persona;
import com.frre.labII.usrs.Profesor;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		List<Persona> personas = new ArrayList();
		personas.add(new Alumno("Jose"));
		personas.add(new Alumno("Carlos"));
		personas.add(new Profesor("Justo" ,34));
		personas.add(new Profesor("Migue",456));
		personas.add(new Alumno("Vargas"));
		personas.add(new Alumno("Marmol"));
		personas.add(new Profesor("Perez", 23));
		personas.add(new Profesor("Juana", 0));
		personas.add(new Profesor("Carlos Jose", -12));
		
		List<Alumno> alumnos = new ArrayList<Alumno>();
		for (Persona per : personas) {
			if (per instanceof Alumno){
				alumnos.add((Alumno) per);
			}
		}
		
		Collections.sort(personas);
		Collections.sort(alumnos);

		//Collections.sort(personas);
		
		System.out.println(personas);
		System.out.println(alumnos);

		/*
		for (Alumno alu : alumnos) {
			mostrar(alu);
		}
		*/
		
		
	}
	
	public static void mostrar(IMostrarPorPantalla pichinga){
		pichinga.mostrarXPant();
	}

}
