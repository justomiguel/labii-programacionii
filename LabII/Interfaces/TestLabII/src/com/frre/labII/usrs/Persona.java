package com.frre.labII.usrs;

public abstract class Persona implements Comparable<Persona>{

	protected String nombre;
	protected int edad;

	
	public Persona(String nombre) {
		super();
		this.nombre = nombre;
	}

	public Persona(String nombre, int edad) {
		super();
		this.nombre = nombre;
		this.edad = edad;
	}



	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public int compareTo(Persona o) {
		return this.nombre.compareTo(o.getNombre())*-1;
	}
	public abstract void vestirse();
}
