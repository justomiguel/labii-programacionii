package com.frre.labII.usrs;

public class Profesor extends Persona {

	private int sueldo;
	
	public Profesor(String nombre, int sueldo) {
		super(nombre);
		this.sueldo = sueldo;
	}
	
	public int getSueldo() {
		return sueldo;
	}

	public void setSueldo(int sueldo) {
		this.sueldo = sueldo;
	}

	public void vestirse() {
		System.out.println("Jogging y Remera");
	}
}
