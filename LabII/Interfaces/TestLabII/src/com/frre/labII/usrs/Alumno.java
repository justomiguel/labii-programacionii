package com.frre.labII.usrs;

import com.frre.labII.IMostrarPorPantalla;

public class Alumno extends Persona implements IMostrarPorPantalla{

	public Alumno(String nombre) {
		super(nombre);
	}

	private int[] cantMateriasSacadas;

	public void vestirse() {
		System.out.println("Jeans y Remera");
	}

	public void mostrarXPant() {
		System.out.println("Soy un ALumno Re Copado!");
	}

	@Override
	public String toString() {
		return "Alumno [nombre=" + nombre + "]";
	}

	
	
	
}
