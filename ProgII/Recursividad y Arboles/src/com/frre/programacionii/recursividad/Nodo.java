package com.frre.programacionii.recursividad;

public class Nodo {

	private int value;
	private Nodo izq;
	private Nodo der;
	
	public Nodo(int value, Nodo izq, Nodo der) {
		super();
		this.value = value;
		this.izq = izq;
		this.der = der;
	}

	public int getValue() {
		return value;
	}

	public Nodo getIzq() {
		return izq;
	}

	public Nodo getDer() {
		return der;
	}
	
	
}
