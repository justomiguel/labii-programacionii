package com.frre.programacionii.recursividad;

import java.io.IOException;

public class MainExcepciones {

	public static void main(String[] args){
		try {
			String data = BaseDeDatos.getData();
			System.out.println(data);
		} catch (IOException e) {
			System.out.println("Se produjo un error. El error es:"+e.getMessage());
		} catch (NoDataException e) {
			System.out.println("No hay data");
		}
		
		
		
	
	}

}
