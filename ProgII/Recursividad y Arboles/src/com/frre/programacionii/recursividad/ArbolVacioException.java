package com.frre.programacionii.recursividad;

public class ArbolVacioException extends RuntimeException{

	public ArbolVacioException(String string) {
		super(string);
	}

}
