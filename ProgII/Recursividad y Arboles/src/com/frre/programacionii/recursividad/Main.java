package com.frre.programacionii.recursividad;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//int[] objs = {3,4,2,3,4,4};
		//recorrer(objs);
		
		/*
		 * 		Nodo x2 = new Nodo(12, null, null);
		Nodo x3 = new Nodo(23, null, null);
		
		Nodo sMas = new Nodo(3, x2, x3);
		
		Nodo x6 = new Nodo(2, null, null);
		
		Nodo sMenos = new Nodo(5, sMas, x6);
		
		Nodo xx6 = new Nodo(4, null, null);
		
		Nodo raiz = new Nodo(1, xx6, sMenos);
		
		System.out.println(getNodoMayor(raiz));
		 */
		
		List<String> lis = new ArrayList<>();
		lis.add("casa");
		lis.add("pepe");
		lis.add("pepe");
		lis.add("casa");
		lis.add("tere");
		lis.add("yerba");
		lis.add("mate");
		lis.add("casa");
		lis.add("yerba");
		
		System.out.println(getOccur(lis));;
	}

	public static int getNodoMayor(Nodo r){
		if (r != null){
			if (isHoja(r)){
				return r.getValue();
			} else {
				return getMayor(Integer.MIN_VALUE, r); 
			}
		}
		throw new ArbolVacioException("Arbol Vacio");
	}
	
	private static int getMayor(int max, Nodo nodo) {
		if (nodo != null){
			if (nodo.getValue() > max){
				max = nodo.getValue();
			}
			return Math.max(getMayor(max, nodo.getIzq()), getMayor(max, nodo.getDer()));
		} else {
			return max;
		}
	}
	
	public static Map<String, Integer> getOccur(List<String> lis){
		HashMap<String, Integer> res = new HashMap<>();
		for (String key : lis) {
			int occur = res.containsKey(key)? res.get(key) + 1 : 1;
			res.put(key, occur);
		}
		return res;
	}
	
	public int countX(String str) {
		  if (str.length() > 0){
		      int count = 0;
		      if (str.charAt(0) == 'x'){
		        count++;
		      }
		      return count+countX(str.substring(1,str.length()));
		  } else {
		      return 0;
		  }
		}


	private static boolean isHoja(Nodo r) {
		return r.getDer()==null && r.getIzq() == null;
	}

	public static void preOrden(Nodo r){
			preOrden(r.getIzq());
			preOrden(r.getDer());
			System.out.println(r.getValue());
	}
	
	public static void recorrer(int[] param){
		recorrer(param, 0);
	}


	private static void recorrer(int[] param, int i) {
		if (i < param.length){
			System.out.println(param[i]);
			recorrer(param, ++i);
		}
	}
}
