package com.frre.programacionii.recursividad;

import java.io.IOException;

public class BaseDeDatos {

	private static String data = null;
	private static boolean on = true;
	
	public static String getData() throws IOException, NoDataException{
		if (on){
			if (data == null){
				throw new NoDataException();
			}
			return data;
		}
		throw new IOException("La Base de Datos esta apagada");
	}
}
