/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Cleo
 */
public interface MedioDeTransporte {
    
    public void acelerar();
    
    public void frenar();
    
    public double getVelocity();

    public void setVelocity(double velocity);
    
    
}
