/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package herencia;

/**
 *
 * @author Cleo
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Moto m = new Moto(58);
        m.acelerar();
        
        AutoDeportivo f = new AutoDeportivo(345);
        f.setTurbo(true);
        f.acelerar();
    }
}
