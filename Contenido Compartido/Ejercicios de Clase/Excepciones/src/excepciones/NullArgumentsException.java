/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author Cleo
 */
public class NullArgumentsException extends MyCustomException{

    
    public NullArgumentsException(String nombreFuncion) {
        super("Se llamo a "+nombreFuncion+" con argumentos nulos");
    }
    
    
}
