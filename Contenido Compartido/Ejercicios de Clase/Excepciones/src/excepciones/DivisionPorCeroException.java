/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author Cleo
 */
public class DivisionPorCeroException extends MyCustomException{

    public final static String MESSAGE = "Division por cero al numero ";
    
    public DivisionPorCeroException(int dividendo) {
        super(MESSAGE+dividendo);
    }
    
    
}
