/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package excepciones;

/**
 *
 * @author Cleo
 */
class Aritmetica {
    
    public static Integer divide(Integer x1, Integer x2) throws NullArgumentsException, DivisionPorCeroException{
        if (x1 == null || x2 == null){
            throw new NullArgumentsException("divide");
        }
        
        if (x1 == 0){
            throw new DivisionPorCeroException(x2);
        }
        return x2/x1;
    }
}
