/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testequals;

/**
 *
 * @author Cleo
 */
public class TestEquals {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws CloneNotSupportedException {
        Monitor m1 = new Monitor();
        m1.setPulgada(17);
        m1.setMarca("Samsung");
        Monitor m2 = new Monitor();
        m2.setPulgada(17);
        m2.setMarca("Samsung");
        Monitor m3 = new Monitor();
        m3.setPulgada(17);
        m3.setMarca("Samsung");
        
        Teclado t = new Teclado();
        t.setCantTeclas(45);
        Teclado t1 = (Teclado) t.clone();
        
        System.out.println(t1.getCantTeclas());
    }
}
