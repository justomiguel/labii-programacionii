/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testequals;

/**
 *
 * @author Cleo
 */
public class Monitor {

    
    private String marca;
    private int pulgada;
    private CircuitoIntegrado circuit;

    public Monitor() {
        circuit = new CircuitoIntegrado();
    }
    

    
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getPulgada() {
        return pulgada;
    }

    public void setPulgada(int pulgada) {
        this.pulgada = pulgada;
    }

    public CircuitoIntegrado getCircuit() {
        return circuit;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null){
            return false;
        }
        if (!(obj instanceof Monitor)){
            return false;
        } 
        Monitor elOtro = (Monitor) obj;
        return (this.pulgada == elOtro.getPulgada()) && 
                (marca.equals(elOtro.getMarca())) &&
                this.circuit.equals(elOtro.getCircuit());
                        
    }
    
    
    
}
