/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package testequals;

/**
 *
 * @author Cleo
 */
public class Teclado implements Cloneable{

    private int cantTeclas;
    
    private CircuitoIntegrado cirt;

    public int getCantTeclas() {
        return cantTeclas;
    }

    public void setCantTeclas(int cantTeclas) {
        this.cantTeclas = cantTeclas;
    }

    public void setCirt(CircuitoIntegrado cirt) {
        this.cirt = cirt;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Teclado t = new Teclado();
        t.setCantTeclas(this.getCantTeclas());
        t.setCirt(new CircuitoIntegrado());
        return t;
    }
    
    
}
