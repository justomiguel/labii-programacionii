/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package practica.colecciones;

import java.util.Objects;

/**
 *
 * @author Cleo
 */
public class Escritor implements Comparable{
    
    private String name;
    private Integer cantLibros;

    public Escritor(String name, Integer cantLibros) {
        this.name = name;
        this.cantLibros = cantLibros;
    }

    public String getName() {
        return name;
    }

    public Integer getCantLibros() {
        return cantLibros;
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Escritor){
            Escritor elOtro = (Escritor) o;
            return this.cantLibros.compareTo(elOtro.getCantLibros());
        }
        return -1;
    }

    boolean esBueno() {
        return true;
    }

    void felicitar() {
        System.out.println("Felicitaciones "+name);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Escritor other = (Escritor) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        if (!Objects.equals(this.cantLibros, other.cantLibros)) {
            return false;
        }
        return true;
    }
    
    
    
}
