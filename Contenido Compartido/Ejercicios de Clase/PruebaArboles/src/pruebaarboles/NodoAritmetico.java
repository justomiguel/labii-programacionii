/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaarboles;

import com.frre.programacion.IBinaryTree;

/**
 *
 * @author Cleo
 */
public class NodoAritmetico implements IBinaryTree{

    private Integer value;
    
    private NodoAritmetico left;
    private NodoAritmetico right;
    
    @Override
    public void setValue(Object o) {
        this.value = (Integer) o;
    }

    @Override
    public Object getValue() {
        return value;
    }

    @Override
    public IBinaryTree getLeftSon() {
        return left;
    }

    @Override
    public IBinaryTree getRighSon() {
        return right;
    }

    @Override
    public void setLeftSon(IBinaryTree ibt) {
        this.left = (NodoAritmetico) ibt;
    }

    @Override
    public void setRighSon(IBinaryTree ibt) {
        this.right = (NodoAritmetico) ibt;
    }
    
    @Override
    public String toString(){
        return value.toString();
    }
}
