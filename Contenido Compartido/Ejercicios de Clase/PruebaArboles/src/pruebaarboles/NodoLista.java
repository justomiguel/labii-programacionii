/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaarboles;

import java.util.Objects;

/**
 *
 * @author Cleo
 */
public class NodoLista {
   
    private Integer value;
    private NodoLista next;

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public NodoLista getNext() {
        return next;
    }

    public void setNext(NodoLista next) {
        this.next = next;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.value);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final NodoLista other = (NodoLista) obj;
        return true;
    }

    @Override
    public String toString() {
        return "NodoLista{" + "value=" + value + '}';
    }
    
    
    
    
}
