/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package pruebaarboles;

import com.frre.programacion.IBinaryTree;
import com.frre.programacion.Utils;

/**
 *
 * @author Cleo
 */
public class PruebaArboles {
    private static NodoLista first;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Object[] enOrden = {1, 7, 12, 11, 0, 6, 5, 7, 21, 32, 11};
        Object[] postOrden = {1, 12, 11, 7, 5,6, 21, 11, 32,7,0};
        
        NodoAritmetico raiz = (NodoAritmetico) Utils.buildTree(NodoAritmetico.class, enOrden, postOrden);
        
        first = null;
        
        getMultplos3((IBinaryTree) raiz);
        //agrega a una lista y muestra multplos de tres
        NodoLista p = first;
        while (p!=null) {            
            //System.out.println(p);
            p = p.getNext();
        }
        
        //
        Integer valorRamaIzquierda = getSumatoria(raiz.getLeftSon());
        Integer valorRamaDerecha = getSumatoria(raiz.getRighSon());
        if (valorRamaDerecha>valorRamaIzquierda){
            System.out.println("Gana la derecha");
        } else {
            System.out.println("Gana la izquierda");
        }
        
        System.out.println("Cant de Niveles o Camino mas largo");
        System.out.println(getCantNiveles(raiz));
    }

    private static void getMultplos3(IBinaryTree nodo) {
        if (nodo != null){
            getMultplos3(nodo.getLeftSon());
            getMultplos3(nodo.getRighSon());
            Integer value = (Integer) nodo.getValue();
            if (value % 3 == 0){
                NodoLista p = new NodoLista();
                p.setValue(value);
                p.setNext(first);
                first = p;
            }
        }
    }
    
    
    private static int getCantNiveles(IBinaryTree nodo){
     if (nodo != null){
         int valorIzq = 1 + getCantNiveles(nodo.getLeftSon());
         int valorDer = 1 + getCantNiveles(nodo.getRighSon());
         return (valorDer > valorIzq)?valorDer:valorIzq;
     } else {
         return 0;
     }   
    }
    
    private static int getSumatoria(IBinaryTree nodo){
        if (nodo != null){
            Integer r = (Integer) nodo.getValue();
            return r +
                    getSumatoria(nodo.getLeftSon()) +
                    getSumatoria(nodo.getRighSon());
        } else {
            return 0;
        }
    }
}
