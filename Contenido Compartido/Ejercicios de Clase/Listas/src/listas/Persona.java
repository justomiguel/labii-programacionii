/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cleo
 */
public class Persona implements Comparable{
    
    private final int dni;
    private final String nomYApe;
    private final int edad;
    private final String pcia;
    
    private List<TelefonoCelular> telefonos;

    public Persona(int dni, String nomYApe, int edad, String pcia) {
        this.dni = dni;
        this.nomYApe = nomYApe;
        this.edad = edad;
        this.pcia = pcia;
        telefonos = new ArrayList<TelefonoCelular>();
    }

    
    public int getDni() {
        return dni;
    }

    public String getNomYApe() {
        return nomYApe;
    }
    
    public void agregarNumero(int numero){
        TelefonoCelular c = new TelefonoCelular();
        c.setNumber(numero);
        telefonos.add(c);
    }

    public int getEdad() {
        return edad;
    }

    public String getPcia() {
        return pcia;
    }

    @Override
    public String toString() {
        return "{"+dni+"}";
    }

    @Override
    public int compareTo(Object o) {
        if (o instanceof Persona){
            Persona elOtro = (Persona) o;
            return this.nomYApe.compareTo(elOtro.getNomYApe());
        } else {
            return -1;
        }
    }
    
    private class TelefonoCelular {
        
        private int number;

        public void setNumber(int number) {
            this.number = number;
        }
        
    }
}
