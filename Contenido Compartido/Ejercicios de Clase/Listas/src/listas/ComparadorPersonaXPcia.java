/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.Comparator;

/**
 *
 * @author Cleo
 */
public class ComparadorPersonaXPcia implements Comparator<Persona>{

    @Override
    public int compare(Persona o1, Persona o2) {
        return o1.getPcia().compareTo(o2.getPcia());
    }
    
}
