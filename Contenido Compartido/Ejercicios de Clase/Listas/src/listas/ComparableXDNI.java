/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listas;

import java.util.Comparator;

/**
 *
 * @author Cleo
 */
public class ComparableXDNI implements Comparator<Persona>{

    @Override
    public int compare(Persona o1, Persona o2) {
       if (o1.getDni() == o2.getDni()){
           return 0;
       } else {
           return o1.getDni() > o2.getDni() ? 1 : -1;
       }
    }
    
}
