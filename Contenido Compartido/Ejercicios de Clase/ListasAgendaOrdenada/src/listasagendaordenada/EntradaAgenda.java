/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listasagendaordenada;

/**
 *
 * @author Cleo
 */
public class EntradaAgenda implements Comparable<EntradaAgenda>{
    
    private String nombre;
    private String apellido;

    public EntradaAgenda(String nombre, String Apellido) {
        this.nombre = nombre;
        this.apellido = Apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String Apellido) {
        this.apellido = Apellido;
    }

    @Override
    public int compareTo(EntradaAgenda otro) {
        int comparacion = this.getApellido().compareTo(otro.getApellido());
        if (comparacion == 0){
            return this.nombre.compareTo(otro.getNombre());
        }
        return comparacion;
    }

    @Override
    public String toString() {
        return "["+apellido+"," + nombre+"]";
    }
    
    
}
