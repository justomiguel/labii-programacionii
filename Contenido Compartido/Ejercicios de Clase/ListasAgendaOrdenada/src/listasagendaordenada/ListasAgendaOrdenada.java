/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package listasagendaordenada;

import com.frre.programacion.Generador;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Cleo
 */
public class ListasAgendaOrdenada {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        List<EntradaAgenda> agenda = new ArrayList<EntradaAgenda>();
        /*for (int i = 0; i < 25; i++) {
            String nombre = Generador.generarNombreAleatorio();
            String apellido = Generador.generarApellidoAleatorio();
            EntradaAgenda record = new EntradaAgenda(nombre, apellido);
            agenda.add(record);
        }*/
        
        agenda.add(new EntradaAgenda("Simon", "Arevalo"));
        agenda.add(new EntradaAgenda("Simon", "Gomez"));
        agenda.add(new EntradaAgenda("Alejandro", "Gomez"));
        
        Collections.sort(agenda);
        
        System.out.println(agenda);
    }
}
