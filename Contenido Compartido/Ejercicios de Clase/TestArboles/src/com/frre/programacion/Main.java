/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.frre.programacion;

/**
 *
 * @author Cleo
 */
public class Main {

    private static Nodo raiz;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Nodo hoja6 = new Nodo();
        hoja6.setDato("6");

        Nodo hoja5 = new Nodo();
        hoja5.setDato("5");

        Nodo hoja7 = new Nodo();
        hoja7.setDato("7");

        Nodo hoja3 = new Nodo();
        hoja3.setDato("3");

        Nodo x = new Nodo();
        x.setDato("x");
        x.setHijoDer(hoja5);
        x.setHijoIzq(hoja6);

        Nodo divi = new Nodo();
        divi.setDato("/");
        divi.setHijoDer(hoja7);
        divi.setHijoIzq(x);

        raiz = new Nodo();
        raiz.setDato("+");
        raiz.setHijoIzq(divi);
        raiz.setHijoDer(hoja3);

        /// ya estoy listo
        postOrden(raiz);
    }

    private static void preOrden(Nodo nodo) {
        if (nodo != null) {
            System.out.print(nodo); //tratar raiz
            preOrden(nodo.getHijoIzq());
            preOrden(nodo.getHijoDer());
        }
    }

    private static void enOrden(Nodo nodo) {
        if (nodo != null) {
            enOrden(nodo.getHijoIzq());
            System.out.print(nodo); //tratar raiz
            enOrden(nodo.getHijoDer());
        }
    }

    private static void postOrden(Nodo nodo) {
        if (nodo != null) {
            postOrden(nodo.getHijoIzq());
            postOrden(nodo.getHijoDer());
            System.out.print(nodo); //tratar raiz
        }
    }
}
